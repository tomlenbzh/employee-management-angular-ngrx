import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { RotateIcon, TogglePanel } from 'src/app/shared/animations/animations';
import { IEmployee } from '../../../utils/employee.interface';

@Component({
  selector: 'app-employee-card',
  templateUrl: './employee-card.component.html',
  styleUrls: ['./employee-card.component.scss'],
  animations: [TogglePanel, RotateIcon]
})
export class EmployeeCardComponent {
  @Input() employee!: IEmployee | null;

  @Output() removed: EventEmitter<IEmployee> = new EventEmitter<IEmployee>();

  showEmployeeInfo: boolean = false;

  constructor(private router: Router) {}

  editProfile(employee: IEmployee): void {
    this.router.navigateByUrl(`app/employees/${employee?.id}`);
  }

  toggleEmployeeCard(): void {
    this.showEmployeeInfo = !this.showEmployeeInfo;
  }
}

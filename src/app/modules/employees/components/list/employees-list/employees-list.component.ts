import { Component, EventEmitter, Input, OnChanges, Output, ViewChild } from '@angular/core';
import { IEmployee } from '../../../utils/employee.interface';
import { SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeFormNames } from '../../../utils/employee.form.constants';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';

@Component({
  selector: 'app-employees-list',
  templateUrl: './employees-list.component.html',
  styleUrls: ['./employees-list.component.scss']
})
export class EmployeesListComponent implements OnChanges {
  @Input() employees!: IEmployee[] | null;

  @Output() removed: EventEmitter<IEmployee> = new EventEmitter<IEmployee>();
  @Output() added: EventEmitter<{ modalId: string; employee: IEmployee }> = new EventEmitter<{
    modalId: string;
    employee: IEmployee;
  }>();

  @ViewChild('modal', { static: true }) private modal!: ModalComponent;

  form!: FormGroup;
  formNames = EmployeeFormNames;

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('employees' in changes && this.employees) this.createForm();
  }

  openModal(): void {
    this.createForm();
    this.modal.open();
  }

  submitForm(): void {
    if (!this.form.valid) return;

    const employee: IEmployee = this.form.getRawValue();
    this.added.emit({ modalId: this.modal.id, employee });
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.formNames.FIRSTNAME]: [null, [Validators.required]],
      [this.formNames.LASTNAME]: [null, [Validators.required]],
      [this.formNames.TITLE]: [null, [Validators.required]],
      [this.formNames.EMAIL]: [null, [Validators.required, Validators.email]],
      [this.formNames.ADDRESS]: [null, []],
      [this.formNames.CITY]: [null, []],
      [this.formNames.SALARY]: [null, []]
    });

    this.form.reset();
  }
}

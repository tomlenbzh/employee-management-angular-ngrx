import { EmployeeCardComponent } from './list/employee-card/employee-card.component';
import { EmployeesListComponent } from './list/employees-list/employees-list.component';
import { EmployeesSingleComponent } from './single/employees-single/employees-single.component';

export const components = [EmployeesListComponent, EmployeesSingleComponent, EmployeeCardComponent];

import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeFormNames } from '../../../utils/employee.form.constants';
import { IEmployee } from '../../../utils/employee.interface';

@Component({
  selector: 'app-employees-single',
  templateUrl: './employees-single.component.html',
  styleUrls: ['./employees-single.component.scss']
})
export class EmployeesSingleComponent implements OnChanges {
  @Input() employee!: IEmployee | null;

  @Output() updated: EventEmitter<IEmployee> = new EventEmitter<IEmployee>();

  form!: FormGroup;
  formNames = EmployeeFormNames;

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('employee' in changes && this.employee) {
      this.createForm();
      this.updateForm();
    }
  }

  updateEmployee(): void {
    if (!this.form.valid) return;

    const employee: IEmployee = { ...this.employee, ...this.form.getRawValue() };
    this.updated.emit(employee);
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.formNames.FIRSTNAME]: [null, [Validators.required]],
      [this.formNames.LASTNAME]: [null, [Validators.required]],
      [this.formNames.TITLE]: [null, [Validators.required]],
      [this.formNames.EMAIL]: [null, [Validators.required, Validators.email]],
      [this.formNames.ADDRESS]: [null, []],
      [this.formNames.CITY]: [null, []],
      [this.formNames.SALARY]: [null, []]
    });
  }

  private updateForm(): void {
    this.form.patchValue({
      [this.formNames.FIRSTNAME]: this.employee?.firstName,
      [this.formNames.LASTNAME]: this.employee?.lastName,
      [this.formNames.TITLE]: this.employee?.title,
      [this.formNames.EMAIL]: this.employee?.email,
      [this.formNames.ADDRESS]: this.employee?.address,
      [this.formNames.CITY]: this.employee?.city,
      [this.formNames.SALARY]: this.employee?.salary
    });
  }
}

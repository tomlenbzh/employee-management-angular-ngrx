import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmployeesListContainerComponent } from './containers/employees-list-container/employees-list-container.component';
import { EmployeesSingleContainerComponent } from './containers/employees-single-container/employees-single-container.component';

const routes: Routes = [
  { path: '', component: EmployeesListContainerComponent },
  { path: ':id', component: EmployeesSingleContainerComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule {}

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { components } from './components';
import { containers } from './containers';
import { EmployeesRoutingModule } from './employees-routing.module';

@NgModule({
  declarations: [...components, ...containers],
  imports: [CommonModule, EmployeesRoutingModule, SharedModule, FormsModule, ReactiveFormsModule, TranslateModule.forChild()]
})
export class EmployeesModule {}

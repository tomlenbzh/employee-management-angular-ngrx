import { ICompany } from '../../auth/utils/company.interface';
import { IDepartment } from '../../departments/utils/department.interface';

export interface IEmployee {
  id?: number;
  firstName?: string;
  lastName?: string;
  company?: ICompany;
  department?: IDepartment | null;
  title?: string;
  address?: string;
  city?: string;
  email?: string;
  salary?: number;
  updatedAt?: Date;
  createdAt?: Date;
}

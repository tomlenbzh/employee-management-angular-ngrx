export enum EmployeeFormNames {
  FIRSTNAME = 'firstName',
  LASTNAME = 'lastName',
  TITLE = 'title',
  EMAIL = 'email',
  ADDRESS = 'address',
  CITY = 'city',
  SALARY = 'salary'
}

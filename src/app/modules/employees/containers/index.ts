import { EmployeesListContainerComponent } from './employees-list-container/employees-list-container.component';
import { EmployeesSingleContainerComponent } from './employees-single-container/employees-single-container.component';

export const containers = [EmployeesListContainerComponent, EmployeesSingleContainerComponent];

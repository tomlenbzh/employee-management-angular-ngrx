import { Component, OnDestroy, OnInit } from '@angular/core';
import { filter, Observable, takeUntil } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { WithDestroy } from 'src/app/shared/mixins/with-destroy';
import { CompanyHelper } from 'src/app/store/company/company.helper';
import { EmployeesListHelper } from 'src/app/store/employees/list/employees-list.helper';
import { IEmployee } from '../../utils/employee.interface';

@Component({
  selector: 'app-employees-list-container',
  template: `<app-employees-list
    [employees]="employees | async"
    (added)="addOneEmployee($event)"
    (removed)="removeOneEmployee($event)"
  ></app-employees-list>`
})
export class EmployeesListContainerComponent extends WithDestroy() implements OnInit, OnDestroy {
  employees!: Observable<IEmployee[] | null>;
  companyId!: number | null;

  constructor(private employeesListHelper: EmployeesListHelper, private companyHelper: CompanyHelper) {
    super();
  }

  ngOnInit(): void {
    this.employees = this.employeesListHelper.employeesList();

    this.companyHelper
      .company()
      .pipe(
        takeUntil(this.destroyed$),
        filter((company: ICompany | null) => company !== null)
      )
      .subscribe((company: ICompany | null) => {
        this.companyId = company?.id!;
        this.employeesListHelper.getAllEmployeesByCompanyId(this.companyId);
      });
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  addOneEmployee(payload: { modalId: string; employee: IEmployee }): void {
    const { modalId, employee } = payload;

    employee.company = { id: this.companyId! };
    this.employeesListHelper.addOneEmployee({ modalId, employee });
  }

  removeOneEmployee(employee: IEmployee): void {
    this.employeesListHelper.removeOneEmployee(employee);
  }
}

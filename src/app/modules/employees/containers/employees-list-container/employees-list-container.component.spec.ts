import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmployeesListContainerComponent } from './employees-list-container.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../../store';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { EmployeesListComponent } from '../../components/list/employees-list/employees-list.component';

describe('EmployeesListContainerComponent', () => {
  let component: EmployeesListContainerComponent;
  let fixture: ComponentFixture<EmployeesListContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeesListContainerComponent, EmployeesListComponent],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeesListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

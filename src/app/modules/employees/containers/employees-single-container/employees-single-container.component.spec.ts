import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterTestingModule } from '@angular/router/testing';
import { StoreModule } from '@ngrx/store';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { EmployeesSingleContainerComponent } from './employees-single-container.component';
import * as fromRoot from '../../../../store';
import { EmployeesSingleComponent } from '../../components/single/employees-single/employees-single.component';

describe('EmployeesSingleContainerComponent', () => {
  let component: EmployeesSingleContainerComponent;
  let fixture: ComponentFixture<EmployeesSingleContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeesSingleContainerComponent, EmployeesSingleComponent],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(EmployeesSingleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

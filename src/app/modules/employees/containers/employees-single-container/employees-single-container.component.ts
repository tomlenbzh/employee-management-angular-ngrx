import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { EmployeeSingleHelper } from 'src/app/store/employees/single/employee-single.helper';
import { IEmployee } from '../../utils/employee.interface';

@Component({
  selector: 'app-employees-single-container',
  template: `<app-employees-single [employee]="employee | async" (updated)="updateEmployee($event)"></app-employees-single>`
})
export class EmployeesSingleContainerComponent implements OnInit {
  employee!: Observable<IEmployee | null>;

  private employeeId!: number;

  constructor(private route: ActivatedRoute, private employeeSingleHelper: EmployeeSingleHelper) {}

  ngOnInit(): void {
    this.employeeId = Number(this.route.snapshot.paramMap.get('id'));
    this.employee = this.employeeSingleHelper.employeeSingle();
    this.employeeSingleHelper.fetchEmployee(this.employeeId);
  }

  updateEmployee(employee: IEmployee): void {
    this.employeeSingleHelper.updateEmployee(employee);
  }
}

export enum UserRole {
  ADMIN = 'admin',
  USER = 'user'
}

export interface ICompany {
  id?: number;
  name?: string;
  email?: string;
  address?: string;
  city?: string;
  businessSector?: string;
  password?: string;
  lang?: string;
}

export interface IPartialCompany {
  id: number;
}

export interface LoginInfo {
  token: string;
  user: ICompany;
}

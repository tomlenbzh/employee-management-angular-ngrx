export enum LoginFormControlsNames {
  EMAIL = 'email',
  PASSWORD = 'password'
}

export enum SignupFormControlsNames {
  USERNAME = 'name',
  EMAIL = 'email',
  PASSWORD = 'password',
  PASSWORD_CONFIRMATION = 'passwordConfirmation'
}

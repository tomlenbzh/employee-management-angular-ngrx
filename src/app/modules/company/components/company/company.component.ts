import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { CompanyFormNames } from '../../utils/company.form.constants';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss']
})
export class CompanyComponent implements OnChanges {
  @Input() company!: ICompany | null;

  @Output() updated: EventEmitter<ICompany> = new EventEmitter<ICompany>();

  form!: FormGroup;
  formNames = CompanyFormNames;

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('company' in changes && this.company) {
      this.createForm();
      this.updateForm();
    }
  }

  submitForm(): void {
    const company: ICompany = { ...this.company, ...this.form.getRawValue() };
    this.updated.emit(company);
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.formNames.NAME]: new FormControl('', [Validators.required]),
      [this.formNames.EMAIL]: new FormControl('', [Validators.required, Validators.email]),
      [this.formNames.ADDRESS]: new FormControl(null, []),
      [this.formNames.CITY]: new FormControl(null, []),
      [this.formNames.BUSINESS_SECTOR]: new FormControl(null, [])
    });
  }

  private updateForm(): void {
    this.form.patchValue({
      [this.formNames.NAME]: this.company?.name!,
      [this.formNames.EMAIL]: this.company?.email!,
      [this.formNames.ADDRESS]: this.company?.address!,
      [this.formNames.CITY]: this.company?.city!,
      [this.formNames.BUSINESS_SECTOR]: this.company?.businessSector!
    });
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'src/app/shared/shared.module';
import { DepartmentListContainerComponent } from '../../containers/departments-list/departments-list-container.component';
import { CompanyComponent } from './company.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../../store';
import { DepartmentsListComponent } from '../departments-list/departments-list.component';
import { TranslateModule } from '@ngx-translate/core';

describe('CompanyComponent', () => {
  let component: CompanyComponent;
  let fixture: ComponentFixture<CompanyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CompanyComponent, DepartmentListContainerComponent, DepartmentsListComponent],
      imports: [SharedModule, FormsModule, ReactiveFormsModule, StoreModule.forRoot(fromRoot.reducers), TranslateModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CompanyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { DepartmentFormNames } from '../../utils/department.form.constants';

@Component({
  selector: 'app-departments-list',
  templateUrl: './departments-list.component.html',
  styleUrls: ['./departments-list.component.scss']
})
export class DepartmentsListComponent {
  @Input() company!: ICompany | null;
  @Input() departments!: IDepartment[] | null;

  @Output() added: EventEmitter<{ modalId: string; department: IDepartment }> = new EventEmitter<{
    modalId: string;
    department: IDepartment;
  }>();
  @Output() removed: EventEmitter<IDepartment> = new EventEmitter<IDepartment>();

  @ViewChild('modal', { static: true }) private modal!: ModalComponent;

  form!: FormGroup;
  formNames = DepartmentFormNames;

  constructor(private formBuilder: FormBuilder, private router: Router) {}

  editDepartment(departmentId: number): void {
    this.router.navigateByUrl(`app/departments/${departmentId}`);
  }

  openModal(): void {
    this.createForm();
    this.modal.open();
  }

  submitForm(): void {
    if (!this.company && !this.form.valid) return;

    const modalId = this.modal.id;
    const company = { id: this.company?.id! };
    const department: IDepartment = { ...this.form.getRawValue(), company };
    const payload = { modalId, department };

    this.added.emit(payload);
  }

  createForm(): void {
    this.form = this.formBuilder.group({
      [this.formNames.NAME]: new FormControl(null, [Validators.required])
    });
  }
}

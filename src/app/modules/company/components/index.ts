import { CompanyComponent } from './company/company.component';
import { DepartmentsListComponent } from './departments-list/departments-list.component';

export const components = [CompanyComponent, DepartmentsListComponent];

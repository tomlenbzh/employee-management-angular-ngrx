export enum CompanyFormNames {
  NAME = 'name',
  EMAIL = 'email',
  ADDRESS = 'address',
  CITY = 'city',
  BUSINESS_SECTOR = 'businessSector'
}

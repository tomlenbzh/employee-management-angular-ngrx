import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { CompanyHelper } from 'src/app/store/company/company.helper';

@Component({
  selector: 'app-company-container',
  template: `<app-company [company]="company | async" (updated)="updateOneCompany($event)"></app-company>`
})
export class CompanyContainerComponent implements OnInit {
  company!: Observable<ICompany | null>;

  constructor(private companyHelper: CompanyHelper) {}

  ngOnInit(): void {
    this.company = this.companyHelper.company();
  }

  updateOneCompany(payload: ICompany): void {
    this.companyHelper.updateCompany(payload);
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CompanyContainerComponent } from './company-container.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../../store';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { CompanyComponent } from '../../components/company/company.component';
import { DepartmentListContainerComponent } from '../departments-list/departments-list-container.component';
import { DepartmentsListComponent } from '../../components/departments-list/departments-list.component';

describe('CompanyContainerComponent', () => {
  let component: CompanyContainerComponent;
  let fixture: ComponentFixture<CompanyContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CompanyContainerComponent, CompanyComponent, DepartmentListContainerComponent, DepartmentsListComponent],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(CompanyContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

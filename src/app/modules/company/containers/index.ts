import { CompanyContainerComponent } from './company/company-container.component';
import { DepartmentListContainerComponent } from './departments-list/departments-list-container.component';

export const containers = [CompanyContainerComponent, DepartmentListContainerComponent];

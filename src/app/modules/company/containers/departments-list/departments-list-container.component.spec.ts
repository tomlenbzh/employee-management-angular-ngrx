import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DepartmentListContainerComponent } from './departments-list-container.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../../store';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { DepartmentsListComponent } from '../../components/departments-list/departments-list.component';

describe('CompanyContainerComponent', () => {
  let component: DepartmentListContainerComponent;
  let fixture: ComponentFixture<DepartmentListContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentListContainerComponent, DepartmentsListComponent],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(DepartmentListContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { DepartmentsListHelper } from 'src/app/store/departments/list/department-list.helper';

@Component({
  selector: 'app-departments-list-container',
  template: `<app-departments-list
    [departments]="departments | async"
    [company]="company"
    (added)="addOneDepartment($event)"
    (removed)="removeOneDepartment($event)"
  ></app-departments-list>`
})
export class DepartmentListContainerComponent implements OnInit, OnChanges {
  @Input() company!: ICompany | null;

  departments!: Observable<IDepartment[] | null>;

  constructor(private departmentsListHelper: DepartmentsListHelper) {}

  ngOnInit(): void {
    this.departments = this.departmentsListHelper.departmentsList();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ('company' in changes && this.company) {
      this.departmentsListHelper.getAllDepartmentsByCompanyId(this.company.id!);
    }
  }

  addOneDepartment(payload: { modalId: string; department: IDepartment }): void {
    const { modalId, department } = payload;
    this.departmentsListHelper.addOnDepartment({ modalId, department });
  }

  removeOneDepartment(department: IDepartment): void {
    this.departmentsListHelper.removeOneDepartment(department);
  }
}

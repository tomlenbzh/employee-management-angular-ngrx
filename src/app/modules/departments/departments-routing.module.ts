import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DepartmentsSingleContainerComponent } from './containers/departments-single-container/departments-single-container.component';

const routes: Routes = [
  { path: ':id', component: DepartmentsSingleContainerComponent },
  { path: '**', redirectTo: '', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DepartmentsRoutingModule {}

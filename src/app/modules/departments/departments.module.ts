import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { components } from './components';
import { containers } from './containers';
import { DepartmentsRoutingModule } from './departments-routing.module';

@NgModule({
  declarations: [...components, ...containers],
  imports: [CommonModule, DepartmentsRoutingModule, SharedModule, FormsModule, ReactiveFormsModule, TranslateModule.forChild()]
})
export class DepartmentsModule {}

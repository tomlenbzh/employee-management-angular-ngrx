import { DepartmentEmployeeFormComponent } from './departments-single/department-employee-form/department-employee-form.component';
import { DepartmentEmployeeComponent } from './departments-single/department-employee/department-employee.component';
import { DepartmentEmployeesListComponent } from './departments-single/department-employees-list/department-employees-list.component';
import { DepartmentsSingleComponent } from './departments-single/departments-single.component';

export const components = [
  DepartmentsSingleComponent,
  DepartmentEmployeeComponent,
  DepartmentEmployeesListComponent,
  DepartmentEmployeeFormComponent
];

import { Component, EventEmitter, Input, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DepartmentFormNames } from 'src/app/modules/company/utils/department.form.constants';
import { IDepartment } from '../../../utils/department.interface';

@Component({
  selector: 'app-department-employee-form',
  templateUrl: './department-employee-form.component.html',
  styleUrls: ['./department-employee-form.component.scss']
})
export class DepartmentEmployeeFormComponent implements OnChanges {
  @Input() department!: IDepartment | null;

  @Output() updated: EventEmitter<IDepartment> = new EventEmitter<IDepartment>();

  form!: FormGroup;
  formNames = DepartmentFormNames;

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('department' in changes && this.department) {
      this.createForm();
      this.updateForm();
    }
  }

  submitForm(): void {
    if (!this.form.valid) return;

    const department: IDepartment = { ...this.department, ...this.form.getRawValue() };
    this.updated.emit(department);
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.formNames.NAME]: new FormControl(null, [Validators.required]),
      [this.formNames.DESCRIPTION]: new FormControl(null, [])
    });
  }

  private updateForm(): void {
    this.form.patchValue({
      [this.formNames.NAME]: this.department?.name!,
      [this.formNames.DESCRIPTION]: this.department?.description!
    });
  }
}

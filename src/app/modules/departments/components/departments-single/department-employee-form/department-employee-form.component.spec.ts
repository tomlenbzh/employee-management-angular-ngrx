import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { DepartmentEmployeeFormComponent } from './department-employee-form.component';

describe('DepartmentEmployeeFormComponent', () => {
  let component: DepartmentEmployeeFormComponent;
  let fixture: ComponentFixture<DepartmentEmployeeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentEmployeeFormComponent],
      imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, SharedModule]
    }).compileComponents();

    fixture = TestBed.createComponent(DepartmentEmployeeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

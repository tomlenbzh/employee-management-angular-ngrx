import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { DepartmentEmployeeFormComponent } from './department-employee-form/department-employee-form.component';
import { DepartmentEmployeesListComponent } from './department-employees-list/department-employees-list.component';
import { DepartmentsSingleComponent } from './departments-single.component';

describe('DepartmentsSingleComponent', () => {
  let component: DepartmentsSingleComponent;
  let fixture: ComponentFixture<DepartmentsSingleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentsSingleComponent, DepartmentEmployeesListComponent, DepartmentEmployeeFormComponent],
      imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, SharedModule, BrowserAnimationsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(DepartmentsSingleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, Input, Output, EventEmitter, ViewChild, SimpleChanges, OnChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeFormNames } from 'src/app/modules/employees/utils/employee.form.constants';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { IDepartment } from '../../utils/department.interface';

@Component({
  selector: 'app-departments-single',
  templateUrl: './departments-single.component.html',
  styleUrls: ['./departments-single.component.scss']
})
export class DepartmentsSingleComponent implements OnChanges {
  @Input() companyId!: number | null;
  @Input() department!: IDepartment | null;
  @Input() departmentEmployees!: IEmployee[] | null;
  @Input() noDepartmentEmployees!: IEmployee[] | null;

  @Output() employeeUpdated: EventEmitter<IEmployee> = new EventEmitter<IEmployee>();
  @Output() departmentUpdated: EventEmitter<IDepartment> = new EventEmitter<IDepartment>();
  @Output() employeeAdded: EventEmitter<{ modalId: string; employee: IEmployee }> = new EventEmitter<{
    modalId: string;
    employee: IEmployee;
  }>();

  @ViewChild('modal', { static: true }) private modal!: ModalComponent;

  form!: FormGroup;
  formNames = EmployeeFormNames;

  constructor(private formBuilder: FormBuilder) {}

  ngOnChanges(changes: SimpleChanges): void {
    if ('department' in changes && this.department) this.createForm();
  }

  openModal(): void {
    this.createForm();
    this.modal.open();
  }

  addNewEmployee(): void {
    if (!this.form.valid) return;

    const employee: IEmployee = {
      ...this.form.getRawValue(),
      company: { id: this.companyId },
      department: { id: this.department?.id }
    };
    this.employeeAdded.emit({ modalId: this.modal.id, employee });
  }

  private createForm(): void {
    this.form = this.formBuilder.group({
      [this.formNames.FIRSTNAME]: [null, [Validators.required]],
      [this.formNames.LASTNAME]: [null, [Validators.required]],
      [this.formNames.TITLE]: [null, [Validators.required]],
      [this.formNames.EMAIL]: [null, [Validators.required, Validators.email]],
      [this.formNames.ADDRESS]: [null, []],
      [this.formNames.CITY]: [null, []],
      [this.formNames.SALARY]: [null, []]
    });

    this.form.reset();
  }
}

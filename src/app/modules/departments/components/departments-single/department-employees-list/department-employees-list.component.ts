import { Component, EventEmitter, Input, OnInit, OnChanges, Output, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatAutocomplete, MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { Router } from '@angular/router';
import { map, Observable, startWith } from 'rxjs';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { IDepartment } from '../../../utils/department.interface';

@Component({
  selector: 'app-department-employees-list',
  templateUrl: './department-employees-list.component.html',
  styleUrls: ['./department-employees-list.component.scss']
})
export class DepartmentEmployeesListComponent implements OnInit, OnChanges {
  @Input() department!: IDepartment | null;
  @Input() departmentEmployees!: IEmployee[] | null;
  @Input() noDepartmentEmployees!: IEmployee[] | null;

  @Output() updated: EventEmitter<IEmployee> = new EventEmitter<IEmployee>();
  @Output() departmentUpdated: EventEmitter<IDepartment> = new EventEmitter<IDepartment>();

  myControl = new FormControl('');
  filteredOptions!: Observable<IEmployee[] | null>;

  constructor(private router: Router) {}

  ngOnInit(): void {
    this.filteredOptions = this.myControl.valueChanges.pipe(map((value) => this._filter(value || '')));
  }

  ngOnChanges(): void {
    this.myControl.patchValue('');
  }

  addExistingEmployee($event: MatAutocompleteSelectedEvent, auto: MatAutocomplete): void {
    this.myControl.patchValue('');
    auto.options.toArray().forEach((item) => item.deselect());

    const employeeId = $event.option.value;
    let employee: IEmployee | undefined = this.noDepartmentEmployees?.find((e: IEmployee) => e?.id === employeeId);

    if (employee) {
      employee = { ...employee, department: { id: this.department?.id! } };
      this.updated.emit(employee);
    }
  }

  private _filter(value: string): IEmployee[] {
    if (typeof value !== 'string') return [];

    const filterValue = value.toLowerCase();
    return (
      this.noDepartmentEmployees!.filter((employee: IEmployee) => {
        const name = `${employee.firstName} ${employee.lastName}`.toLowerCase();
        return name.includes(filterValue);
      }) || []
    );
  }
}

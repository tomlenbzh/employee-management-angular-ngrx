import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { SharedModule } from 'src/app/shared/shared.module';
import { DepartmentEmployeesListComponent } from './department-employees-list.component';

describe('DepartmentEmployeesListComponent', () => {
  let component: DepartmentEmployeesListComponent;
  let fixture: ComponentFixture<DepartmentEmployeesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DepartmentEmployeesListComponent],
      imports: [TranslateModule.forRoot(), FormsModule, ReactiveFormsModule, SharedModule, BrowserAnimationsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(DepartmentEmployeesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

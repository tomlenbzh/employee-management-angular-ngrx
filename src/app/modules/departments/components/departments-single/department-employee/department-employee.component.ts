import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { IDepartment } from '../../../utils/department.interface';

@Component({
  selector: 'app-department-employee',
  templateUrl: './department-employee.component.html',
  styleUrls: ['./department-employee.component.scss']
})
export class DepartmentEmployeeComponent {
  @Input() employee!: IEmployee;
  @Input() department!: IDepartment | null;
  @Input() departmentEmployees!: IEmployee[] | null;

  @Output() departmentUpdated: EventEmitter<IDepartment> = new EventEmitter<IDepartment>();
  @Output() updated: EventEmitter<IEmployee> = new EventEmitter<IEmployee>();

  constructor(private router: Router) {}

  get hasManager(): boolean {
    return this.department?.manager ? true : false;
  }

  get isManager(): boolean {
    if (!this.hasManager) return false;
    return this.department?.manager?.id === this.employee.id;
  }

  removeManager(): void {
    if (!this.department) return;

    const department = Object.assign({}, this.department);
    department!.manager = null;
    this.departmentUpdated.emit(department);
  }

  setAsManager(employee: IEmployee) {
    const department: IDepartment = { ...this.department, manager: { id: employee?.id } };
    this.departmentUpdated.emit(department);
  }

  editEmployee(employee: IEmployee): void {
    this.router.navigateByUrl(`app/employees/${employee.id}`);
  }

  removeEmployee($event: IEmployee): void {
    if (!$event?.department) return;

    const employee = Object.assign({}, $event);
    employee!.department = null;
    this.updated.emit(employee);

    const isManager = this.isManager;
    isManager && this.removeManager();
  }
}

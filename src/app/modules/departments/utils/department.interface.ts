import { ICompany } from '../../auth/utils/company.interface';
import { IEmployee } from '../../employees/utils/employee.interface';

export interface IDepartment {
  id?: number;
  name?: string;
  description?: string;
  manager?: IEmployee | null;
  company?: ICompany;
  // employees?: IEmployee[];
  employeesCount?: number;
  createdAt?: Date;
  updatedAt?: Date;
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DepartmentsSingleContainerComponent } from './departments-single-container.component';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../../store';
import { SharedModule } from 'src/app/shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateModule } from '@ngx-translate/core';
import { DepartmentsSingleComponent } from '../../components/departments-single/departments-single.component';
import { DepartmentEmployeesListComponent } from '../../components/departments-single/department-employees-list/department-employees-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DepartmentEmployeeFormComponent } from '../../components/departments-single/department-employee-form/department-employee-form.component';

describe('DepartmentsSingleContainerComponent', () => {
  let component: DepartmentsSingleContainerComponent;
  let fixture: ComponentFixture<DepartmentsSingleContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        DepartmentsSingleContainerComponent,
        DepartmentsSingleComponent,
        DepartmentEmployeesListComponent,
        DepartmentEmployeeFormComponent
      ],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(DepartmentsSingleContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

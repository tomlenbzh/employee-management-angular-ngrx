import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter, Observable, takeUntil } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { WithDestroy } from 'src/app/shared/mixins/with-destroy';
import { CompanyHelper } from 'src/app/store/company/company.helper';
import { DepartmentSingleHelper } from 'src/app/store/departments/single/department-single.helper';
import { EmployeesListHelper } from 'src/app/store/employees/list/employees-list.helper';
import { IDepartment } from '../../utils/department.interface';

@Component({
  selector: 'app-departments-single-container',
  template: `<app-departments-single
    [companyId]="companyId"
    [department]="department | async"
    [departmentEmployees]="departmentEmployees | async"
    [noDepartmentEmployees]="noDepartmentEmployees | async"
    (employeeUpdated)="updateOneEmployee($event)"
    (departmentUpdated)="updateOneDepartment($event)"
    (employeeAdded)="addOneEmployee($event)"
  ></app-departments-single>`
})
export class DepartmentsSingleContainerComponent extends WithDestroy() implements OnInit, OnDestroy {
  company!: Observable<ICompany | null>;
  department!: Observable<IDepartment | null>;
  departmentEmployees!: Observable<IEmployee[] | null>;
  noDepartmentEmployees!: Observable<IEmployee[] | null>;
  companyId!: number | null;

  private departmentId!: number;

  constructor(
    private route: ActivatedRoute,
    private companyHelper: CompanyHelper,
    private employeesListHelper: EmployeesListHelper,
    private departmentSingleHelper: DepartmentSingleHelper
  ) {
    super();
  }

  ngOnInit(): void {
    this.departmentId = Number(this.route.snapshot.paramMap.get('id'));
    this.department = this.departmentSingleHelper.departmentSingle();
    this.departmentSingleHelper.fetchDepartment(this.departmentId);

    this.departmentEmployees = this.employeesListHelper.employeesListByDepartment(this.departmentId);
    this.noDepartmentEmployees = this.employeesListHelper.employeesListNotInDepartment();

    this.companyHelper
      .company()
      .pipe(
        takeUntil(this.destroyed$),
        filter((company: ICompany | null) => company !== null)
      )
      .subscribe((company: ICompany | null) => {
        this.companyId = company?.id!;
        this.employeesListHelper.getAllEmployeesByCompanyId(this.companyId);
      });
  }

  override ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  updateOneDepartment(department: IDepartment): void {
    this.departmentSingleHelper.updateDepartment(department);
  }

  updateOneEmployee(employee: IEmployee): void {
    this.employeesListHelper.updateOneEmployee({ modalId: '', employee });
  }

  addOneEmployee(payload: { modalId: string; employee: IEmployee }): void {
    const { modalId, employee } = payload;
    this.employeesListHelper.addOneEmployee({ modalId, employee });
  }
}

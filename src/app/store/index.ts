import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { Action } from '@ngrx/store/src/models';

import { environment } from '../../environments/environment';
import { AuthActionTypes } from './auth/auth.actions.types';

import { AuthState, authReducer } from './auth/auth.reducer';
import { CompanyState, companyReducer } from './company/company.reducer';
import { EmployeeListState, employeeListReducer } from './employees/list/employees-list.reducer';
import { DepartmentSingleState, departmentSingleReducer } from './departments/single/department-single.reducer';
import { EmployeeSingleState, employeeSingleReducer } from './employees/single/employee-single.reducer';

import * as fromDepartmentList from './departments/list/department-list.reducer';

export interface AppState {
  auth: AuthState;
  company: CompanyState;
  employeeSingle: EmployeeSingleState;
  employeesList: EmployeeListState;
  departmentSingle: DepartmentSingleState;
  departmentsList: fromDepartmentList.DepartmentListState;
}

export const reducers: ActionReducerMap<AppState> = {
  auth: authReducer,
  company: companyReducer,
  employeeSingle: employeeSingleReducer,
  employeesList: employeeListReducer,
  departmentSingle: departmentSingleReducer,
  departmentsList: fromDepartmentList.departmentListReducer
};

export const metaReducers: MetaReducer<any>[] = !environment.production ? [clearState] : [clearState];

export function clearState(reducer: (arg0: any, arg1: any) => any) {
  return (state: any, action: Action) => {
    if (action.type === AuthActionTypes.LOGOUT) state = undefined;
    return reducer(state, action);
  };
}

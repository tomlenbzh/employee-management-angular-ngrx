import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import {
  FETCH_COMPANY_ACTION,
  FETCH_COMPANY_ACTION_ERROR,
  FETCH_COMPANY_ACTION_SUCCESS,
  UPDATE_COMPANY_ACTION,
  UPDATE_COMPANY_ACTION_ERROR,
  UPDATE_COMPANY_ACTION_SUCCESS
} from './company.actions';
import { TranslateService } from '@ngx-translate/core';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { CompanyService } from 'src/app/services/rest/company.service';
import { ToasterService } from 'src/app/services/components/toaster.service';

@Injectable()
export class CompanyEffects {
  constructor(private actions$: Actions, private companyService: CompanyService, private toasterService: ToasterService) {}

  fetchUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_COMPANY_ACTION),
      exhaustMap((action) =>
        this.companyService.fetchCompany(action.id).pipe(
          map((company: ICompany) => FETCH_COMPANY_ACTION_SUCCESS({ company })),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while fetching the company information');
            return of(FETCH_COMPANY_ACTION_ERROR({ error }));
          })
        )
      )
    );
  });

  updateUser$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_COMPANY_ACTION),
      exhaustMap((action) =>
        this.companyService.updateCompany(action.company).pipe(
          map((company: ICompany) => {
            this.toasterService.success('Success', 'Information successfully updated !');
            return UPDATE_COMPANY_ACTION_SUCCESS({ company });
          }),
          catchError((error) => {
            this.toasterService.error('Error', "An error occurred while updating your company's information");
            return of(UPDATE_COMPANY_ACTION_ERROR({ error }));
          })
        )
      )
    );
  });
}

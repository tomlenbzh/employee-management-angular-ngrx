export enum CompanyActionTypes {
  FETCH_COMPANY = '[COMPANY] Fetch company',
  FETCH_COMPANY_SUCCESS = '[COMPANY] Fetch company Success',
  FETCH_COMPANY_ERROR = '[COMPANY] Fetch company Error',
  UPDATE_COMPANY = '[COMPANY] Update company',
  UPDATE_COMPANY_SUCCESS = '[COMPANY] Update company Success',
  UPDATE_COMPANY_ERROR = '[COMPANY] Update company Error'
}

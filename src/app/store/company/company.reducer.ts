import { createReducer, on } from '@ngrx/store';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import {
  FETCH_COMPANY_ACTION,
  FETCH_COMPANY_ACTION_ERROR,
  FETCH_COMPANY_ACTION_SUCCESS,
  UPDATE_COMPANY_ACTION,
  UPDATE_COMPANY_ACTION_ERROR,
  UPDATE_COMPANY_ACTION_SUCCESS
} from './company.actions';

export interface CompanyState {
  isLoading: boolean;
  company: ICompany | null;
}

export const initialState: CompanyState = {
  isLoading: false,
  company: null
};

export const companyReducer = createReducer(
  initialState,
  on(FETCH_COMPANY_ACTION, (state: CompanyState): CompanyState => ({ ...state, isLoading: true })),
  on(FETCH_COMPANY_ACTION_SUCCESS, (state: CompanyState, { company }): CompanyState => ({ ...state, isLoading: false, company })),
  on(FETCH_COMPANY_ACTION_ERROR, (state: CompanyState): CompanyState => ({ ...state, isLoading: false })),
  on(UPDATE_COMPANY_ACTION, (state: CompanyState): CompanyState => ({ ...state, isLoading: true })),
  on(
    UPDATE_COMPANY_ACTION_SUCCESS,
    (state: CompanyState, { company }): CompanyState => ({ ...state, isLoading: false, company })
  ),
  on(UPDATE_COMPANY_ACTION_ERROR, (state: CompanyState): CompanyState => ({ ...state, isLoading: false }))
);

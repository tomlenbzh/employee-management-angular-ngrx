import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { CompanyActionTypes } from './company.actions.types';

export const FETCH_COMPANY_ACTION = createAction(CompanyActionTypes.FETCH_COMPANY, props<{ id: number }>());
export const FETCH_COMPANY_ACTION_SUCCESS = createAction(
  CompanyActionTypes.FETCH_COMPANY_SUCCESS,
  props<{ company: ICompany }>()
);
export const FETCH_COMPANY_ACTION_ERROR = createAction(
  CompanyActionTypes.FETCH_COMPANY_ERROR,
  props<{ error: HttpErrorResponse }>()
);

export const UPDATE_COMPANY_ACTION = createAction(CompanyActionTypes.UPDATE_COMPANY, props<{ company: ICompany }>());
export const UPDATE_COMPANY_ACTION_SUCCESS = createAction(
  CompanyActionTypes.UPDATE_COMPANY_SUCCESS,
  props<{ company: ICompany }>()
);
export const UPDATE_COMPANY_ACTION_ERROR = createAction(
  CompanyActionTypes.UPDATE_COMPANY_ERROR,
  props<{ error: HttpErrorResponse }>()
);

import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { CompanyState } from './company.reducer';

export const companyFeature = (state: AppState) => state.company;

export const selectCompany = createSelector(companyFeature, (state: CompanyState) => state?.company);
export const selectCompanyLoading = createSelector(companyFeature, (state: CompanyState) => state.isLoading);

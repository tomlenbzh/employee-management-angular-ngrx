import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { AppState } from 'src/app/store';
import { FETCH_COMPANY_ACTION, UPDATE_COMPANY_ACTION } from './company.actions';
import { selectCompany, selectCompanyLoading } from './company.selectors';

@Injectable({ providedIn: 'root' })
export class CompanyHelper {
  constructor(private store: Store<AppState>) {}

  isLoading(): Observable<boolean> {
    return this.store.select(selectCompanyLoading);
  }

  company(): Observable<ICompany | null> {
    return this.store.select(selectCompany);
  }

  fetchCompany(id: number): void {
    this.store.dispatch(FETCH_COMPANY_ACTION({ id }));
  }

  updateCompany(company: ICompany): void {
    this.store.dispatch(UPDATE_COMPANY_ACTION({ company }));
  }
}

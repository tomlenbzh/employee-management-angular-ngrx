import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { EmployeesListActionTypes } from './employees-list.actions.types';

/**
 * FETCH ACTIONS
 */
export const FETCH_ALL_EMPLOYEES_LIST_ACTION = createAction(
  EmployeesListActionTypes.FETCH_ALL_EMPLOYEES_LIST,
  props<{ companyId: number }>()
);
export const FETCH_ALL_EMPLOYEES_LIST_SUCCESS_ACTION = createAction(
  EmployeesListActionTypes.FETCH_ALL_EMPLOYEES_LIST_SUCCESS,
  props<{ employees: IEmployee[] }>()
);
export const FETCH_ALL_EMPLOYEES_LIST_ERROR_ACTION = createAction(
  EmployeesListActionTypes.FETCH_ALL_EMPLOYEES_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * ADD ACTIONS
 */
export const ADD_ONE_EMPLOYEE_LIST_ACTION = createAction(
  EmployeesListActionTypes.ADD_ONE_EMPLOYEES_LIST,
  props<{ modalId: string; employee: IEmployee }>()
);
export const ADD_ONE_EMPLOYEE_LIST_SUCCESS_ACTION = createAction(
  EmployeesListActionTypes.ADD_ONE_EMPLOYEES_LIST_SUCCESS,
  props<{ employee: IEmployee }>()
);
export const ADD_ONE_EMPLOYEE_LIST_ERROR_ACTION = createAction(
  EmployeesListActionTypes.ADD_ONE_EMPLOYEES_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * UPDATE ACTIONS
 */
export const UPDATE_ONE_EMPLOYEE_LIST_ACTION = createAction(
  EmployeesListActionTypes.UPDATE_ONE_EMPLOYEES_LIST,
  props<{ modalId: string; employee: IEmployee }>()
);
export const UPDATE_ONE_EMPLOYEE_LIST_SUCCESS_ACTION = createAction(
  EmployeesListActionTypes.UPDATE_ONE_EMPLOYEES_LIST_SUCCESS,
  props<{ id: number; changes: IEmployee }>()
);
export const UPDATE_ONE_EMPLOYEE_LIST_ERROR_ACTION = createAction(
  EmployeesListActionTypes.UPDATE_ONE_EMPLOYEES_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * DELETE ACTIONS
 */
export const REMOVE_ONE_EMPLOYEES_LIST_ACTION = createAction(
  EmployeesListActionTypes.REMOVE_ONE_EMPLOYEES_LIST,
  props<{ employee: IEmployee }>()
);
export const REMOVE_ONE_EMPLOYEES_LIST_SUCCESS_ACTION = createAction(
  EmployeesListActionTypes.REMOVE_ONE_EMPLOYEES_LIST_SUCCESS,
  props<{ id: number }>()
);
export const REMOVE_ONE_EMPLOYEES_LIST_ERROR_ACTION = createAction(
  EmployeesListActionTypes.REMOVE_ONE_EMPLOYEES_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

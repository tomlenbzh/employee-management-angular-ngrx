import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromEmployeesList from './employees-list.reducer';

export const selectDepartmentState = createFeatureSelector<fromEmployeesList.EmployeeListState>('employeesList');

export const selectEmployeeIds = createSelector(selectDepartmentState, fromEmployeesList.selectEmployeeIds);

export const selectEmployeeEntities = createSelector(selectDepartmentState, fromEmployeesList.selectEmployeeEntities);

export const selectAllEmployees = createSelector(selectDepartmentState, fromEmployeesList.selectAllEmployees);

export const selectEmployeesTotal = createSelector(selectDepartmentState, fromEmployeesList.selectEmployeesTotal);

export const selectDepartmentListId = createSelector(selectDepartmentState, fromEmployeesList.getSelectedEmployeeListId);

export const selectCurrentTask = createSelector(
  selectEmployeeEntities,
  selectDepartmentListId,
  (EmployeeEntities, EmployeeId) => EmployeeId && EmployeeEntities[EmployeeId]
);

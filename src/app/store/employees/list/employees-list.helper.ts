import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { filter, map, Observable } from 'rxjs';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { AppState } from '../..';
import {
  ADD_ONE_EMPLOYEE_LIST_ACTION,
  FETCH_ALL_EMPLOYEES_LIST_ACTION,
  REMOVE_ONE_EMPLOYEES_LIST_ACTION,
  UPDATE_ONE_EMPLOYEE_LIST_ACTION
} from './employees-list.actions';
import { selectAllEmployees } from './employees-list.selectors';

@Injectable({ providedIn: 'root' })
export class EmployeesListHelper {
  constructor(private store: Store<AppState>) {}

  employeesList(): Observable<IEmployee[] | null> {
    return this.store.select(selectAllEmployees);
  }

  employeesListByDate(): Observable<IEmployee[] | null> {
    return this.employeesList().pipe(
      map(
        (employees: IEmployee[] | null) =>
          employees?.sort((a: IEmployee, b: IEmployee) => new Date(b.updatedAt!).getTime() - new Date(a.updatedAt!).getTime()) ||
          []
      )
    );
  }

  employeesListByDepartment(departmentId: number): Observable<IEmployee[] | null> {
    return this.employeesListByDate().pipe(
      map((employees: IEmployee[] | null) => employees?.filter((employee) => employee.department?.id === departmentId) || [])
    );
  }

  employeesListNotInDepartment(): Observable<IEmployee[] | null> {
    return this.employeesListByDate().pipe(
      map((employees: IEmployee[] | null) => employees?.filter((employee) => !employee.department) || [])
    );
  }

  addOneEmployeeToDepartment(payload: { modalId: string; employee: IEmployee }): void {
    const { modalId, employee } = payload;
    this.store.dispatch(ADD_ONE_EMPLOYEE_LIST_ACTION({ modalId, employee }));
  }

  addOneEmployee(payload: { modalId: string; employee: IEmployee }): void {
    const { modalId, employee } = payload;
    this.store.dispatch(ADD_ONE_EMPLOYEE_LIST_ACTION({ modalId, employee }));
  }

  updateOneEmployee(payload: { modalId: string; employee: IEmployee }): void {
    const { modalId, employee } = payload;
    this.store.dispatch(UPDATE_ONE_EMPLOYEE_LIST_ACTION({ modalId, employee }));
  }

  removeOneEmployee(employee: IEmployee): void {
    this.store.dispatch(REMOVE_ONE_EMPLOYEES_LIST_ACTION({ employee }));
  }

  getAllEmployeesByCompanyId(companyId: number): void {
    this.store.dispatch(FETCH_ALL_EMPLOYEES_LIST_ACTION({ companyId }));
  }
}

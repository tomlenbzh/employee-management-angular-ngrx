import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, mergeMap, of, tap } from 'rxjs';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { ModalRegisterService } from 'src/app/services/components/modal-components.service';
import { ToasterService } from 'src/app/services/components/toaster.service';
import { EmployeeService } from 'src/app/services/rest/employee.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import {
  FETCH_ALL_EMPLOYEES_LIST_ACTION,
  FETCH_ALL_EMPLOYEES_LIST_ERROR_ACTION,
  FETCH_ALL_EMPLOYEES_LIST_SUCCESS_ACTION,
  UPDATE_ONE_EMPLOYEE_LIST_ACTION,
  UPDATE_ONE_EMPLOYEE_LIST_SUCCESS_ACTION,
  UPDATE_ONE_EMPLOYEE_LIST_ERROR_ACTION,
  ADD_ONE_EMPLOYEE_LIST_ACTION,
  ADD_ONE_EMPLOYEE_LIST_SUCCESS_ACTION,
  ADD_ONE_EMPLOYEE_LIST_ERROR_ACTION,
  REMOVE_ONE_EMPLOYEES_LIST_ACTION,
  REMOVE_ONE_EMPLOYEES_LIST_SUCCESS_ACTION,
  REMOVE_ONE_EMPLOYEES_LIST_ERROR_ACTION
} from './employees-list.actions';

@Injectable()
export class EmployeesListEffects {
  constructor(
    private actions$: Actions,
    private employeesListService: EmployeeService,
    private modalRegisterService: ModalRegisterService,
    private toasterService: ToasterService
  ) {}

  getAllEmployeesByCompanyId$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_ALL_EMPLOYEES_LIST_ACTION),
      exhaustMap((action) =>
        this.employeesListService.getAllEmployeesByCompanyId(action.companyId).pipe(
          map((employees) => FETCH_ALL_EMPLOYEES_LIST_SUCCESS_ACTION({ employees })),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while fetching the employees !');
            return of(FETCH_ALL_EMPLOYEES_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  updateOneEmployee$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_ONE_EMPLOYEE_LIST_ACTION),
      exhaustMap((action) =>
        this.employeesListService.updateOneEmployee(action.employee).pipe(
          map((employee) => {
            this.toasterService.success('Success', 'Employee successfully updated !');
            return UPDATE_ONE_EMPLOYEE_LIST_SUCCESS_ACTION({ id: employee.id!, changes: employee });
          }),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while updating the employee !');
            return of(UPDATE_ONE_EMPLOYEE_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  addOneEmployee$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ADD_ONE_EMPLOYEE_LIST_ACTION),
      exhaustMap((action) =>
        this.employeesListService.addOneEmployee(action.employee).pipe(
          tap(() => {
            const modal: ModalComponent = this.modalRegisterService.getComponent(action.modalId);
            modal && this.modalRegisterService.deregisterComponent(action.modalId);
            modal && modal.close();
          }),
          map((employee: IEmployee) => {
            this.toasterService.success('Success', 'Employee successfully added !');
            return ADD_ONE_EMPLOYEE_LIST_SUCCESS_ACTION({ employee });
          }),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while creating the employee !');
            return of(ADD_ONE_EMPLOYEE_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  removeOneEmployee$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(REMOVE_ONE_EMPLOYEES_LIST_ACTION),
      exhaustMap((action) =>
        this.employeesListService.removeOneEmployee(action.employee).pipe(
          map(() => {
            this.toasterService.success('Success', 'Employee successfully deleted !');
            return REMOVE_ONE_EMPLOYEES_LIST_SUCCESS_ACTION({ id: action.employee.id! });
          }),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while deleting the employee !');
            return of(REMOVE_ONE_EMPLOYEES_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });
}

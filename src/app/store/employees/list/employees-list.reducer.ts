import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import {
  ADD_ONE_EMPLOYEE_LIST_SUCCESS_ACTION,
  FETCH_ALL_EMPLOYEES_LIST_SUCCESS_ACTION,
  REMOVE_ONE_EMPLOYEES_LIST_SUCCESS_ACTION,
  UPDATE_ONE_EMPLOYEE_LIST_SUCCESS_ACTION
} from './employees-list.actions';

export interface EmployeeListState extends EntityState<IEmployee> {
  selectedEmployeeId: string | null;
}

export const adapter = createEntityAdapter<IEmployee>({
  selectId: (employee: IEmployee) => employee.id!
});

export const initialState: EmployeeListState = adapter.getInitialState({ selectedEmployeeId: null });

export const employeeListReducer = createReducer(
  initialState,
  on(FETCH_ALL_EMPLOYEES_LIST_SUCCESS_ACTION, (state: EmployeeListState, { employees }) => adapter.setAll(employees, state)),
  on(ADD_ONE_EMPLOYEE_LIST_SUCCESS_ACTION, (state: EmployeeListState, { employee }) => adapter.addOne(employee, state)),
  on(UPDATE_ONE_EMPLOYEE_LIST_SUCCESS_ACTION, (state: EmployeeListState, { id, changes }) =>
    adapter.updateOne({ id, changes }, state)
  ),
  on(REMOVE_ONE_EMPLOYEES_LIST_SUCCESS_ACTION, (state: EmployeeListState, { id }) => adapter.removeOne(id, state))
);

export const getSelectedEmployeeListId = (state: EmployeeListState) => state.selectedEmployeeId;

const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const selectEmployeeIds = selectIds;
export const selectEmployeeEntities = selectEntities;
export const selectAllEmployees = selectAll;
export const selectEmployeesTotal = selectTotal;

import { createReducer, on } from '@ngrx/store';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import {
  FETCH_SINGLE_EMPLOYEE_ACTION,
  FETCH_SINGLE_EMPLOYEE_ERROR_ACTION,
  FETCH_SINGLE_EMPLOYEE_SUCCESS_ACTION,
  UPDATE_SINGLE_EMPLOYEE_ACTION,
  UPDATE_SINGLE_EMPLOYEE_ERROR_ACTION,
  UPDATE_SINGLE_EMPLOYEE_SUCCESS_ACTION
} from './employee-single.actions';

export interface EmployeeSingleState {
  isLoading: boolean;
  employee: IDepartment | null;
}

export const initialState: EmployeeSingleState = {
  isLoading: false,
  employee: null
};

export const employeeSingleReducer = createReducer(
  initialState,
  on(FETCH_SINGLE_EMPLOYEE_ACTION, (state: EmployeeSingleState): EmployeeSingleState => ({ ...state, isLoading: true })),
  on(
    FETCH_SINGLE_EMPLOYEE_SUCCESS_ACTION,
    (state: EmployeeSingleState, { employee }): EmployeeSingleState => ({ ...state, isLoading: false, employee })
  ),
  on(FETCH_SINGLE_EMPLOYEE_ERROR_ACTION, (state: EmployeeSingleState): EmployeeSingleState => ({ ...state, isLoading: false })),
  on(UPDATE_SINGLE_EMPLOYEE_ACTION, (state: EmployeeSingleState): EmployeeSingleState => ({ ...state, isLoading: true })),
  on(
    UPDATE_SINGLE_EMPLOYEE_SUCCESS_ACTION,
    (state: EmployeeSingleState, { employee }): EmployeeSingleState => ({ ...state, isLoading: false, employee })
  ),
  on(UPDATE_SINGLE_EMPLOYEE_ERROR_ACTION, (state: EmployeeSingleState): EmployeeSingleState => ({ ...state, isLoading: false }))
);

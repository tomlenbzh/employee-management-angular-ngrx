import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { EmployeeSingleActionTypes } from './employee-single.actions.types';

/**
 * FETCH ACTIONS
 */
export const FETCH_SINGLE_EMPLOYEE_ACTION = createAction(
  EmployeeSingleActionTypes.FETCH_SINGLE_EMPLOYEE,
  props<{ employeeId: number }>()
);
export const FETCH_SINGLE_EMPLOYEE_SUCCESS_ACTION = createAction(
  EmployeeSingleActionTypes.FETCH_SINGLE_EMPLOYEE_SUCCESS,
  props<{ employee: IEmployee }>()
);
export const FETCH_SINGLE_EMPLOYEE_ERROR_ACTION = createAction(
  EmployeeSingleActionTypes.FETCH_SINGLE_EMPLOYEE_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * UPDATE ACTIONS
 */
export const UPDATE_SINGLE_EMPLOYEE_ACTION = createAction(
  EmployeeSingleActionTypes.UPDATE_ONE_SINGLE_EMPLOYEE,
  props<{ modalId: string; employee: IEmployee }>()
);
export const UPDATE_SINGLE_EMPLOYEE_SUCCESS_ACTION = createAction(
  EmployeeSingleActionTypes.UPDATE_ONE_SINGLE_EMPLOYEE_SUCCESS,
  props<{ employee: IEmployee }>()
);
export const UPDATE_SINGLE_EMPLOYEE_ERROR_ACTION = createAction(
  EmployeeSingleActionTypes.UPDATE_ONE_SINGLE_EMPLOYEE_ERROR,
  props<{ error: HttpErrorResponse }>()
);

import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { AppState } from 'src/app/store';
import { FETCH_SINGLE_EMPLOYEE_ACTION, UPDATE_SINGLE_EMPLOYEE_ACTION } from './employee-single.actions';
import { selectEmployeeSingle, selectEmployeeSingleLoading } from './employee-single.selectors';

@Injectable({ providedIn: 'root' })
export class EmployeeSingleHelper {
  constructor(private store: Store<AppState>) {}

  isLoading(): Observable<boolean> {
    return this.store.select(selectEmployeeSingleLoading);
  }

  employeeSingle(): Observable<IEmployee | null> {
    return this.store.select(selectEmployeeSingle);
  }

  fetchEmployee(employeeId: number): void {
    this.store.dispatch(FETCH_SINGLE_EMPLOYEE_ACTION({ employeeId }));
  }

  updateEmployee(employee: IEmployee): void {
    this.store.dispatch(UPDATE_SINGLE_EMPLOYEE_ACTION({ modalId: '', employee }));
  }
}

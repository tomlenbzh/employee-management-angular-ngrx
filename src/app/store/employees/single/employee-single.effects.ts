import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { EmployeeService } from 'src/app/services/rest/employee.service';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import {
  FETCH_SINGLE_EMPLOYEE_ACTION,
  FETCH_SINGLE_EMPLOYEE_ERROR_ACTION,
  FETCH_SINGLE_EMPLOYEE_SUCCESS_ACTION,
  UPDATE_SINGLE_EMPLOYEE_ACTION,
  UPDATE_SINGLE_EMPLOYEE_ERROR_ACTION,
  UPDATE_SINGLE_EMPLOYEE_SUCCESS_ACTION
} from './employee-single.actions';
import { ToasterService } from 'src/app/services/components/toaster.service';

@Injectable()
export class EmployeeSingleEffects {
  constructor(private actions$: Actions, private employeeService: EmployeeService, private toasterService: ToasterService) {}

  fetchEmployee$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_SINGLE_EMPLOYEE_ACTION),
      exhaustMap((action) =>
        this.employeeService.getOneEmployee(action.employeeId).pipe(
          map((employee: IEmployee) => FETCH_SINGLE_EMPLOYEE_SUCCESS_ACTION({ employee })),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while fetching the employee !');
            return of(FETCH_SINGLE_EMPLOYEE_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  updateEmployee$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_SINGLE_EMPLOYEE_ACTION),
      exhaustMap((action) =>
        this.employeeService.updateOneEmployee(action.employee).pipe(
          map((employee: IEmployee) => {
            this.toasterService.success('Success', 'Employee successfully updated !');
            return UPDATE_SINGLE_EMPLOYEE_SUCCESS_ACTION({ employee });
          }),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while updating the employee !');
            return of(UPDATE_SINGLE_EMPLOYEE_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });
}

import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { EmployeeSingleState } from './employee-single.reducer';

export const employeeFeature = (state: AppState) => state.employeeSingle;

export const selectEmployeeSingle = createSelector(employeeFeature, (state: EmployeeSingleState) => state?.employee);
export const selectEmployeeSingleLoading = createSelector(employeeFeature, (state: EmployeeSingleState) => state.isLoading);

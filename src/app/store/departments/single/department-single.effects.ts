import { Injectable } from '@angular/core';
import { Actions, ofType, createEffect } from '@ngrx/effects';
import { of } from 'rxjs';
import { catchError, exhaustMap, map } from 'rxjs/operators';
import { DepartmentService } from 'src/app/services/rest/department.service';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import {
  FETCH_SINGLE_DEPARTMENT_ACTION,
  FETCH_SINGLE_DEPARTMENT_ERROR_ACTION,
  FETCH_SINGLE_DEPARTMENT_SUCCESS_ACTION,
  UPDATE_SINGLE_DEPARTMENT_ACTION,
  UPDATE_SINGLE_DEPARTMENT_ERROR_ACTION,
  UPDATE_SINGLE_DEPARTMENT_SUCCESS_ACTION
} from './department-single.actions';
import { ToasterService } from 'src/app/services/components/toaster.service';

@Injectable()
export class DepartmentSingleEffects {
  constructor(private actions$: Actions, private departmentService: DepartmentService, private toasterService: ToasterService) {}

  fetchDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_SINGLE_DEPARTMENT_ACTION),
      exhaustMap((action) =>
        this.departmentService.getOneDepartment(action.departmentId).pipe(
          map((department: IDepartment) => FETCH_SINGLE_DEPARTMENT_SUCCESS_ACTION({ department })),
          catchError((error) => {
            this.toasterService.error('Error', "An error occurred while fetching the department's information !");
            return of(FETCH_SINGLE_DEPARTMENT_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  updateDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_SINGLE_DEPARTMENT_ACTION),
      exhaustMap((action) =>
        this.departmentService.updateOneDepartment(action.department).pipe(
          map((department: IDepartment) => {
            this.toasterService.success('Success', 'Department successfully updated !');
            return UPDATE_SINGLE_DEPARTMENT_SUCCESS_ACTION({ department });
          }),
          catchError((error) => {
            this.toasterService.error('Error', "An error occurred while updating the department's information !");
            return of(UPDATE_SINGLE_DEPARTMENT_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });
}

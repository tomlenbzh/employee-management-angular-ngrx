import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { AppState } from 'src/app/store';
import { FETCH_SINGLE_DEPARTMENT_ACTION, UPDATE_SINGLE_DEPARTMENT_ACTION } from './department-single.actions';
import { selectDepartmentSingle, selectDepartmentSingleLoading } from './department-single.selectors';

@Injectable({ providedIn: 'root' })
export class DepartmentSingleHelper {
  constructor(private store: Store<AppState>) {}

  isLoading(): Observable<boolean> {
    return this.store.select(selectDepartmentSingleLoading);
  }

  departmentSingle(): Observable<IDepartment | null> {
    return this.store.select(selectDepartmentSingle);
  }

  fetchDepartment(departmentId: number): void {
    this.store.dispatch(FETCH_SINGLE_DEPARTMENT_ACTION({ departmentId }));
  }

  updateDepartment(department: IDepartment): void {
    this.store.dispatch(UPDATE_SINGLE_DEPARTMENT_ACTION({ modalId: '', department }));
  }
}

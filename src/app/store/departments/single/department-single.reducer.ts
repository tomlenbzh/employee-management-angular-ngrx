import { createReducer, on } from '@ngrx/store';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import {
  FETCH_SINGLE_DEPARTMENT_ACTION,
  FETCH_SINGLE_DEPARTMENT_ERROR_ACTION,
  FETCH_SINGLE_DEPARTMENT_SUCCESS_ACTION,
  UPDATE_SINGLE_DEPARTMENT_ACTION,
  UPDATE_SINGLE_DEPARTMENT_ERROR_ACTION,
  UPDATE_SINGLE_DEPARTMENT_SUCCESS_ACTION
} from './department-single.actions';

export interface DepartmentSingleState {
  isLoading: boolean;
  department: IDepartment | null;
}

export const initialState: DepartmentSingleState = {
  isLoading: false,
  department: null
};

export const departmentSingleReducer = createReducer(
  initialState,
  on(FETCH_SINGLE_DEPARTMENT_ACTION, (state: DepartmentSingleState): DepartmentSingleState => ({ ...state, isLoading: true })),
  on(
    FETCH_SINGLE_DEPARTMENT_SUCCESS_ACTION,
    (state: DepartmentSingleState, { department }): DepartmentSingleState => ({ ...state, isLoading: false, department })
  ),
  on(
    FETCH_SINGLE_DEPARTMENT_ERROR_ACTION,
    (state: DepartmentSingleState): DepartmentSingleState => ({ ...state, isLoading: false })
  ),
  on(UPDATE_SINGLE_DEPARTMENT_ACTION, (state: DepartmentSingleState): DepartmentSingleState => ({ ...state, isLoading: true })),
  on(
    UPDATE_SINGLE_DEPARTMENT_SUCCESS_ACTION,
    (state: DepartmentSingleState, { department }): DepartmentSingleState => ({ ...state, isLoading: false, department })
  ),
  on(
    UPDATE_SINGLE_DEPARTMENT_ERROR_ACTION,
    (state: DepartmentSingleState): DepartmentSingleState => ({ ...state, isLoading: false })
  )
);

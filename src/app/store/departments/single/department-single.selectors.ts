import { createSelector } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { DepartmentSingleState } from './department-single.reducer';

export const departmentFeature = (state: AppState) => state.departmentSingle;

export const selectDepartmentSingle = createSelector(departmentFeature, (state: DepartmentSingleState) => state?.department);
export const selectDepartmentSingleLoading = createSelector(departmentFeature, (state: DepartmentSingleState) => state.isLoading);

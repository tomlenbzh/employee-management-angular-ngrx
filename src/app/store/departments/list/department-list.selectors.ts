import { createSelector, createFeatureSelector } from '@ngrx/store';
import * as fromDepartmentList from './department-list.reducer';

export const selectDepartmentState = createFeatureSelector<fromDepartmentList.DepartmentListState>('departmentsList');

export const selectDepartmentIds = createSelector(selectDepartmentState, fromDepartmentList.selectDepartmentIds);

export const selectDepartmentEntities = createSelector(selectDepartmentState, fromDepartmentList.selectDepartmentEntities);

export const selectAllDepartments = createSelector(selectDepartmentState, fromDepartmentList.selectAllDepartments);

export const selectDepartmentTotal = createSelector(selectDepartmentState, fromDepartmentList.selectDepartmentTotal);

export const selectDepartmentListId = createSelector(selectDepartmentState, fromDepartmentList.getSelectedDepartmentListId);

export const selectCurrentTask = createSelector(
  selectDepartmentEntities,
  selectDepartmentListId,
  (DepartmentEntities, DepartmentId) => DepartmentId && DepartmentEntities[DepartmentId]
);

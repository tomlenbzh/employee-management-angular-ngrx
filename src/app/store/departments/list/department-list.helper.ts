import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { AppState } from '../..';
import {
  ADD_ONE_DEPARTMENT_LIST_ACTION,
  FETCH_ALL_DEPARTMENT_LIST_ACTION,
  REMOVE_ONE_DEPARTMENT_LIST_ACTION,
  UPDATE_ONE_DEPARTMENT_LIST_ACTION
} from './department-list.actions';
import { selectAllDepartments } from './department-list.selectors';

@Injectable({ providedIn: 'root' })
export class DepartmentsListHelper {
  constructor(private store: Store<AppState>) {}

  departmentsList(): Observable<IDepartment[] | null> {
    return this.store.select(selectAllDepartments);
  }

  addOnDepartment(payload: { modalId: string; department: IDepartment }): void {
    const { modalId, department } = payload;
    this.store.dispatch(ADD_ONE_DEPARTMENT_LIST_ACTION({ modalId, department }));
  }

  updateOneDepartment(payload: { modalId: string; department: IDepartment }): void {
    const { modalId, department } = payload;
    this.store.dispatch(UPDATE_ONE_DEPARTMENT_LIST_ACTION({ modalId, department }));
  }

  removeOneDepartment(department: IDepartment): void {
    this.store.dispatch(REMOVE_ONE_DEPARTMENT_LIST_ACTION({ department }));
  }

  getAllDepartmentsByCompanyId(companyId: number): void {
    this.store.dispatch(FETCH_ALL_DEPARTMENT_LIST_ACTION({ companyId }));
  }
}

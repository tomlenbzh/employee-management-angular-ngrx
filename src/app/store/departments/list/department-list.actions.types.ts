export enum DepartmentListActionTypes {
  FETCH_ALL_DEPARTMENT_LIST = '[DEPARTMENT_LIST] Fetch all',
  FETCH_ALL_DEPARTMENT_LIST_SUCCESS = '[DEPARTMENT_LIST] Fetch all Success',
  FETCH_ALL_DEPARTMENT_LIST_ERROR = '[DEPARTMENT_LIST] Fetch Error',
  ADD_ONE_DEPARTMENT_LIST = '[DEPARTMENT_LIST] Add one',
  ADD_ONE_DEPARTMENT_LIST_SUCCESS = '[DEPARTMENT_LIST] Add one Success',
  ADD_ONE_DEPARTMENT_LIST_ERROR = '[DEPARTMENT_LIST] Add one Error',
  UPDATE_ONE_DEPARTMENT_LIST = '[DEPARTMENT_LIST] Update one',
  UPDATE_ONE_DEPARTMENT_LIST_SUCCESS = '[DEPARTMENT_LIST] Update one Success',
  UPDATE_ONE_DEPARTMENT_LIST_ERROR = '[DEPARTMENT_LIST] Update one Error',
  REMOVE_ONE_DEPARTMENT_LIST = '[DEPARTMENT_LIST] Delete one',
  REMOVE_ONE_DEPARTMENT_LIST_SUCCESS = '[DEPARTMENT_LIST] Delete one Success',
  REMOVE_ONE_DEPARTMENT_LIST_ERROR = '[DEPARTMENT_LIST] Delete one Error'
}

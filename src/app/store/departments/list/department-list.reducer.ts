import { createEntityAdapter, EntityState } from '@ngrx/entity';
import { createReducer, on } from '@ngrx/store';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import {
  FETCH_ALL_DEPARTMENT_LIST_SUCCESS_ACTION,
  ADD_ONE_DEPARTMENT_LIST_SUCCESS_ACTION,
  UPDATE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION,
  REMOVE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION
} from './department-list.actions';

export interface DepartmentListState extends EntityState<IDepartment> {
  selecteDepartmentId: string | null;
}

export const adapter = createEntityAdapter<IDepartment>({
  selectId: (department: IDepartment) => department.id!
});

export const initialState: DepartmentListState = adapter.getInitialState({ selecteDepartmentId: null });

export const departmentListReducer = createReducer(
  initialState,
  on(FETCH_ALL_DEPARTMENT_LIST_SUCCESS_ACTION, (state: DepartmentListState, { departments }) =>
    adapter.setAll(departments, state)
  ),
  on(ADD_ONE_DEPARTMENT_LIST_SUCCESS_ACTION, (state: DepartmentListState, { department }) => adapter.addOne(department, state)),
  on(UPDATE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION, (state: DepartmentListState, { id, changes }) =>
    adapter.updateOne({ id, changes }, state)
  ),
  on(REMOVE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION, (state: DepartmentListState, { id }) => adapter.removeOne(id, state))
);

export const getSelectedDepartmentListId = (state: DepartmentListState) => state.selecteDepartmentId;

const { selectIds, selectEntities, selectAll, selectTotal } = adapter.getSelectors();

export const selectDepartmentIds = selectIds;
export const selectDepartmentEntities = selectEntities;
export const selectAllDepartments = selectAll;
export const selectDepartmentTotal = selectTotal;

import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { catchError, exhaustMap, map, mergeMap, of, tap } from 'rxjs';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { ModalRegisterService } from 'src/app/services/components/modal-components.service';
import { ToasterService } from 'src/app/services/components/toaster.service';
import { DepartmentService } from 'src/app/services/rest/department.service';
import { ModalComponent } from 'src/app/shared/components/modal/modal.component';
import { FETCH_ALL_EMPLOYEES_LIST_ACTION } from '../../employees/list/employees-list.actions';
import {
  ADD_ONE_DEPARTMENT_LIST_ACTION,
  ADD_ONE_DEPARTMENT_LIST_ERROR_ACTION,
  ADD_ONE_DEPARTMENT_LIST_SUCCESS_ACTION,
  FETCH_ALL_DEPARTMENT_LIST_ACTION,
  FETCH_ALL_DEPARTMENT_LIST_ERROR_ACTION,
  FETCH_ALL_DEPARTMENT_LIST_SUCCESS_ACTION,
  REMOVE_ONE_DEPARTMENT_LIST_ACTION,
  REMOVE_ONE_DEPARTMENT_LIST_ERROR_ACTION,
  REMOVE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION,
  UPDATE_ONE_DEPARTMENT_LIST_ACTION,
  UPDATE_ONE_DEPARTMENT_LIST_ERROR_ACTION,
  UPDATE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION
} from './department-list.actions';

@Injectable()
export class DepartmentsListEffects {
  constructor(
    private actions$: Actions,
    private departmentService: DepartmentService,
    private modalRegisterService: ModalRegisterService,
    private toasterService: ToasterService
  ) {}

  addOneDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(ADD_ONE_DEPARTMENT_LIST_ACTION),
      exhaustMap((action) =>
        this.departmentService.addOneDepartment(action.department).pipe(
          tap(() => {
            const modal: ModalComponent = this.modalRegisterService.getComponent(action.modalId);
            modal && this.modalRegisterService.deregisterComponent(action.modalId);
            modal && modal.close();
          }),
          mergeMap((department: IDepartment) => [
            ADD_ONE_DEPARTMENT_LIST_SUCCESS_ACTION({ department }),
            FETCH_ALL_EMPLOYEES_LIST_ACTION({ companyId: department?.company?.id! })
          ]),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while adding a department !');
            return of(ADD_ONE_DEPARTMENT_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  updateOneDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(UPDATE_ONE_DEPARTMENT_LIST_ACTION),
      exhaustMap((action) =>
        this.departmentService.updateOneDepartment(action.department).pipe(
          map((department) => {
            this.toasterService.success('Success', 'Department information successfully updated !');
            return UPDATE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION({ id: department.id!, changes: department });
          }),
          catchError((error) => {
            this.toasterService.error('Error', "An error occurred while updating department`'s information !");
            return of(UPDATE_ONE_DEPARTMENT_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  removeOneDepartment$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(REMOVE_ONE_DEPARTMENT_LIST_ACTION),
      exhaustMap((action) =>
        this.departmentService.removeOneDepartment(action.department).pipe(
          map(() => {
            this.toasterService.success('Success', 'Department information successfully deleted !');
            return REMOVE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION({ id: action.department.id! });
          }),
          catchError((error) => {
            this.toasterService.error('Error', 'An error occurred while deleting the department !');
            return of(REMOVE_ONE_DEPARTMENT_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });

  getAllDepartmentsByCompanyId$ = createEffect(() => {
    return this.actions$.pipe(
      ofType(FETCH_ALL_DEPARTMENT_LIST_ACTION),
      exhaustMap((action) =>
        this.departmentService.getAllDepartmentsByCompanyId(action.companyId).pipe(
          map((departments) => FETCH_ALL_DEPARTMENT_LIST_SUCCESS_ACTION({ departments })),
          catchError((error) => {
            this.toasterService.error('Error', "An error occurred while fetching your company's departments !");
            return of(FETCH_ALL_DEPARTMENT_LIST_ERROR_ACTION({ error }));
          })
        )
      )
    );
  });
}

import { HttpErrorResponse } from '@angular/common/http';
import { createAction, props } from '@ngrx/store';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { DepartmentListActionTypes } from './department-list.actions.types';

/**
 * FETCH ACTIONS
 */
export const FETCH_ALL_DEPARTMENT_LIST_ACTION = createAction(
  DepartmentListActionTypes.FETCH_ALL_DEPARTMENT_LIST,
  props<{ companyId: number }>()
);
export const FETCH_ALL_DEPARTMENT_LIST_SUCCESS_ACTION = createAction(
  DepartmentListActionTypes.FETCH_ALL_DEPARTMENT_LIST_SUCCESS,
  props<{ departments: IDepartment[] }>()
);
export const FETCH_ALL_DEPARTMENT_LIST_ERROR_ACTION = createAction(
  DepartmentListActionTypes.FETCH_ALL_DEPARTMENT_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * ADD ACTIONS
 */
export const ADD_ONE_DEPARTMENT_LIST_ACTION = createAction(
  DepartmentListActionTypes.ADD_ONE_DEPARTMENT_LIST,
  props<{ modalId: string; department: IDepartment }>()
);
export const ADD_ONE_DEPARTMENT_LIST_SUCCESS_ACTION = createAction(
  DepartmentListActionTypes.ADD_ONE_DEPARTMENT_LIST_SUCCESS,
  props<{ department: IDepartment }>()
);
export const ADD_ONE_DEPARTMENT_LIST_ERROR_ACTION = createAction(
  DepartmentListActionTypes.ADD_ONE_DEPARTMENT_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * UPDATE ACTIONS
 */
export const UPDATE_ONE_DEPARTMENT_LIST_ACTION = createAction(
  DepartmentListActionTypes.UPDATE_ONE_DEPARTMENT_LIST,
  props<{ modalId: string; department: IDepartment }>()
);
export const UPDATE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION = createAction(
  DepartmentListActionTypes.UPDATE_ONE_DEPARTMENT_LIST_SUCCESS,
  props<{ id: number; changes: IDepartment }>()
);
export const UPDATE_ONE_DEPARTMENT_LIST_ERROR_ACTION = createAction(
  DepartmentListActionTypes.UPDATE_ONE_DEPARTMENT_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

/**
 * DELETE ACTIONS
 */
export const REMOVE_ONE_DEPARTMENT_LIST_ACTION = createAction(
  DepartmentListActionTypes.REMOVE_ONE_DEPARTMENT_LIST,
  props<{ department: IDepartment }>()
);
export const REMOVE_ONE_DEPARTMENT_LIST_SUCCESS_ACTION = createAction(
  DepartmentListActionTypes.REMOVE_ONE_DEPARTMENT_LIST_SUCCESS,
  props<{ id: number }>()
);
export const REMOVE_ONE_DEPARTMENT_LIST_ERROR_ACTION = createAction(
  DepartmentListActionTypes.REMOVE_ONE_DEPARTMENT_LIST_ERROR,
  props<{ error: HttpErrorResponse }>()
);

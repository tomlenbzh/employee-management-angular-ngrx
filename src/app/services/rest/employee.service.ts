import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { IEmployee } from 'src/app/modules/employees/utils/employee.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class EmployeeService {
  constructor(private httpClient: HttpClient) {}

  getAllEmployeesByCompanyId(companyId: number): Observable<IEmployee[]> {
    return this.httpClient.get<IDepartment[]>(`${environment.baseUrl}/employees/company/${companyId}`);
  }

  getOneEmployee(employeeId: number): Observable<IEmployee> {
    return this.httpClient.get<IDepartment>(`${environment.baseUrl}/employees/${employeeId}`);
  }

  addOneEmployee(employee: IEmployee): Observable<IEmployee> {
    return this.httpClient.post<IEmployee>(`${environment.baseUrl}/employees`, employee);
  }

  updateOneEmployee(employee: IEmployee): Observable<IEmployee> {
    return this.httpClient.put<IEmployee>(`${environment.baseUrl}/employees/${employee.id}`, employee);
  }

  removeOneEmployee(employee: IEmployee): Observable<IEmployee> {
    return this.httpClient.delete<IEmployee>(`${environment.baseUrl}/employees/${employee.id}`);
  }
}

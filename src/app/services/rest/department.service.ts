import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { IDepartment } from 'src/app/modules/departments/utils/department.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class DepartmentService {
  constructor(private httpClient: HttpClient) {}

  getAllDepartmentsByCompanyId(companyId: number): Observable<IDepartment[]> {
    return this.httpClient.get<IDepartment[]>(`${environment.baseUrl}/departments/company/${companyId}`);
  }

  getOneDepartment(departmentId: number): Observable<IDepartment> {
    return this.httpClient.get<IDepartment>(`${environment.baseUrl}/departments/${departmentId}`);
  }

  addOneDepartment(department: IDepartment): Observable<IDepartment> {
    return this.httpClient.post<IDepartment>(`${environment.baseUrl}/departments`, department);
  }

  updateOneDepartment(department: IDepartment): Observable<IDepartment> {
    return this.httpClient.put<IDepartment>(`${environment.baseUrl}/departments/${department.id}`, department);
  }

  removeOneDepartment(department: IDepartment): Observable<IDepartment> {
    return this.httpClient.delete<IDepartment>(`${environment.baseUrl}/departments/${department.id}`);
  }
}

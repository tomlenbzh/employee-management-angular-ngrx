import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthService {
  constructor(private httpClient: HttpClient) {}

  login(credentials: ICompany): Observable<any> {
    const url = `${environment.baseUrl}/companies/login`;
    return this.httpClient.post<any>(url, credentials);
  }

  signUp(credentials: ICompany): Observable<any> {
    const url = `${environment.baseUrl}/companies`;
    return this.httpClient.post<any>(url, credentials);
  }
}

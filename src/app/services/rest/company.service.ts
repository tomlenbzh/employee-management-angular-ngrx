import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class CompanyService {
  constructor(private httpClient: HttpClient) {}

  fetchCompany(id: number): Observable<ICompany> {
    return this.httpClient.get<ICompany>(`${environment.baseUrl}/companies/${id}`);
  }

  updateCompany(company: ICompany): Observable<ICompany> {
    return this.httpClient.put<ICompany>(`${environment.baseUrl}/companies/${company.id}`, company);
  }
}

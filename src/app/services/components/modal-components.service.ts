import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class ModalRegisterService {
  private componentRegistry: Map<string, any> = new Map<string, any>();

  /**
   * Register navigation component
   *
   * @param     { string }      name
   * @param     { any }         component
   */
  registerComponent(name: string, component: any): void {
    this.componentRegistry.set(name, component);
  }

  /**
   * Deregister navigation component
   *
   * @param     { string }      name
   */
  deregisterComponent(name: string): void {
    this.componentRegistry.delete(name);
  }

  /**
   * Get navigation component from the registry
   *
   * @param     { string }      name
   * @returns   { T }
   */
  getComponent<T>(name: string): T {
    return this.componentRegistry.get(name);
  }
}

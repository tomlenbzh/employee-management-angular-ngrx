import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({ providedIn: 'root' })
export class ToasterService {
  constructor(private toastr: ToastrService) {}

  error(title: string, message: string): void {
    this.toastr.error(title, message, { timeOut: 2000 });
  }

  success(title: string, message: string): void {
    this.toastr.success(title, message, { timeOut: 2000 });
  }
}

export const matTwoToneIcons = 'assets/icons/material-twotone.svg';
export const matOutlineIcons = 'assets/icons/material-outline.svg';
export const matSolidIcons = 'assets/icons/material-solid.svg';
export const featherIcons = 'assets/icons/feather.svg';
export const heroicOutline = 'assets/icons/heroicons-outline.svg';
export const heroicSolid = 'assets/icons/heroicons-solid.svg';

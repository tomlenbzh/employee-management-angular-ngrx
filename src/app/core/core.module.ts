import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule, DomSanitizer } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from '../app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StoreModule } from '@ngrx/store';
import { environment } from 'src/environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule, RouterState } from '@ngrx/router-store';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { setMatIcons } from '../utils/methods/set-icons';
import { MatIconRegistry } from '@angular/material/icon';
import { SharedModule } from '../shared/shared.module';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { LANG } from '../utils/constants/lang';
import { TokenInterceptor } from '../shared/interceptors/headers.interceptor';
import { UnauthorizedInterceptor } from '../shared/interceptors/unauthorized.interceptor';
import { reducers, metaReducers } from '../store';
import { AuthEffects } from '../store/auth/auth.effects';
import { CompanyEffects } from '../store/company/company.effects';
import { components } from './components';
import { containers } from './containers';
import { DepartmentsListEffects } from '../store/departments/list/department-list.effects';
import { DepartmentSingleEffects } from '../store/departments/single/department-single.effects';
import { EmployeesListEffects } from '../store/employees/list/employees-list.effects';
import { EmployeeSingleEffects } from '../store/employees/single/employee-single.effects';
import { ToastrModule } from 'ngx-toastr';

const StoreEffects: any[] = [
  AuthEffects,
  CompanyEffects,
  DepartmentsListEffects,
  DepartmentSingleEffects,
  EmployeesListEffects,
  EmployeeSingleEffects
];

@NgModule({
  declarations: [AppComponent, ...components, ...containers],
  imports: [
    CommonModule,
    BrowserModule.withServerTransition({ appId: 'app' }),
    BrowserAnimationsModule,
    HttpClientModule,
    TranslateModule.forRoot({
      defaultLanguage: LANG.FR,
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      },
      isolate: false
    }),
    NgbModule,
    SharedModule,
    RouterModule,
    ToastrModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({ maxAge: 25, logOnly: environment.production }),
    EffectsModule.forRoot([...StoreEffects]),
    StoreRouterConnectingModule.forRoot({ routerState: RouterState.Minimal })
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnauthorizedInterceptor,
      multi: true
    }
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() parentModule: CoreModule,
    private domSanitizer: DomSanitizer,
    private matIconRegistry: MatIconRegistry
  ) {
    setMatIcons(this.matIconRegistry, this.domSanitizer);

    if (parentModule) {
      throw new Error('CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders<CoreModule> {
    return {
      ngModule: CoreModule,
      providers: []
    };
  }
}

// required for AOT compilation
export function HttpLoaderFactory(http: HttpClient): TranslateHttpLoader {
  return new TranslateHttpLoader(http);
}

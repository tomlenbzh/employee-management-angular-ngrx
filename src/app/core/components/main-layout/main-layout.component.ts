import { Component, Input, SimpleChanges, OnChanges, ViewChild, Output, EventEmitter } from '@angular/core';
import { MatDrawer, MatDrawerMode, MatSidenav } from '@angular/material/sidenav';
import { TranslateService } from '@ngx-translate/core';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';
import { ACCEPTED_LANGUAGES, LANG, SELECTED_LANGUAGE } from 'src/app/utils/constants/lang';
import { MenuItems } from '../../utils/menu.constants';
import { MenuItem } from '../../utils/menu.interface';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss']
})
export class MainLayoutComponent implements OnChanges {
  @Input() isOnTop: boolean = true;
  @Input() isLargeViewport: boolean = true;
  @Input() company!: ICompany | null;

  @Output() loggedOut: EventEmitter<any> = new EventEmitter<any>();
  @Output() langChanged: EventEmitter<ICompany> = new EventEmitter<ICompany>();

  @ViewChild('drawer') drawer!: MatDrawer;
  @ViewChild('sidenav') sidenav!: MatSidenav;

  fixed: boolean = true;
  opened: boolean = true;
  top: number = 0;
  bottom: number = 0;
  mode: MatDrawerMode = 'side';
  initial: string = '';
  menuItems: MenuItem[] = MenuItems;

  acceptedLangs = ACCEPTED_LANGUAGES;

  constructor(private translate: TranslateService) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnChanges(changes: SimpleChanges): void {
    if ('company' in changes && this.company) {
      this.initial = this.company.name![0];
      this.setCurrentLang();
    }

    if ('isLargeViewport' in changes) {
      if (this.isLargeViewport && this.sidenav?.opened) this.toggleDrawer();
    }
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  toggleDrawer(): void {
    this.drawer.toggle();
  }

  logout(): void {
    this.loggedOut.emit();
  }

  changeLang(lang: LANG): void {
    const company: ICompany = { ...this.company, lang };
    this.langChanged.emit(company);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private setCurrentLang(): void {
    this.translate.setDefaultLang(this.company?.lang!);
    this.translate.use(this.company?.lang!);

    localStorage.removeItem(SELECTED_LANGUAGE);
    localStorage.setItem(SELECTED_LANGUAGE, this.company?.lang!);
  }
}

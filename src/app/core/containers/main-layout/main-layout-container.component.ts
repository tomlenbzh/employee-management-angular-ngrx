import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Component, AfterViewInit, OnDestroy, OnInit } from '@angular/core';
import { AuthHelper } from 'src/app/store/auth/auth.helper';
import { Router } from '@angular/router';
import jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs';
import { CompanyHelper } from 'src/app/store/company/company.helper';
import { ICompany } from 'src/app/modules/auth/utils/company.interface';

@Component({
  selector: 'app-main-layout-container',
  template: `<app-main-layout
    [company]="company | async"
    [isLargeViewport]="isLargeViewport"
    (loggedOut)="logout()"
    (langChanged)="updateCompany($event)"
  ></app-main-layout>`
})
export class MainLayoutContainerComponent implements AfterViewInit, OnDestroy, OnInit {
  company!: Observable<ICompany | null>;

  isOnTop = true;
  isLargeViewport = false;

  private smallBreakPoint = '(min-width: 768px)';

  constructor(
    public breakpointObserver: BreakpointObserver,
    private authHelper: AuthHelper,
    private companyHelper: CompanyHelper,
    private router: Router
  ) {}

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle Hooks
  // -----------------------------------------------------------------------------------------------------

  ngOnInit() {
    this.company = this.companyHelper.company();
    this.fetchCompany();

    this.breakpointObserver
      .observe([this.smallBreakPoint])
      .subscribe((state: BreakpointState) => (this.isLargeViewport = state.matches));
  }

  ngAfterViewInit(): void {
    window.addEventListener('scroll', this.onScroll.bind(this), true);
  }

  ngOnDestroy(): void {
    window.removeEventListener('scroll', this.onScroll, true);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Public methods
  // -----------------------------------------------------------------------------------------------------

  logout(): void {
    this.authHelper.logout();
  }

  updateCompany(company: any): void {
    this.companyHelper.updateCompany(company as ICompany);
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Private methods
  // -----------------------------------------------------------------------------------------------------

  private fetchCompany(): void {
    const token: string | null = this.authHelper.getAccessToken();

    if (token) {
      const companyId: number | undefined = this.decodeToken(token);
      companyId && this.companyHelper.fetchCompany(companyId);
    } else {
      this.router.navigateByUrl('auth');
    }
  }

  /**
   * Checks if has scrolled pas 50px in viewport.
   *
   * @param       { any }      event
   */
  private onScroll(event: any): void {
    if (event.srcElement.scrollTop > 50) {
      this.isOnTop = false;
    } else {
      this.isOnTop = true;
    }
  }

  /**
   * Returns the user id from the acees token.
   *
   * @param     { string }      token
   * @returns   { number | undefined }
   */
  private decodeToken(token: string): number | undefined {
    const decoded: any = jwt_decode(token);
    const company: ICompany = decoded?.company;
    return company.id;
  }
}

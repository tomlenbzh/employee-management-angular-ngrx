import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SharedModule } from 'src/app/shared/shared.module';
import { MainLayoutComponent } from '../../components/main-layout/main-layout.component';
import { MainLayoutContainerComponent } from './main-layout-container.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { setMatIcons } from 'src/app/utils/methods/set-icons';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { TranslateModule } from '@ngx-translate/core';
import { StoreModule } from '@ngrx/store';
import * as fromRoot from '../../../store';

describe('MainLayoutContainerComponent', () => {
  let component: MainLayoutContainerComponent;
  let fixture: ComponentFixture<MainLayoutContainerComponent>;
  let domSanitizer: DomSanitizer;
  let matIconRegistry: MatIconRegistry;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [MainLayoutContainerComponent, MainLayoutComponent],
      imports: [
        RouterTestingModule,
        SharedModule,
        HttpClientModule,
        BrowserAnimationsModule,
        TranslateModule.forRoot(),
        StoreModule.forRoot(fromRoot.reducers)
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(MainLayoutContainerComponent);
    component = fixture.componentInstance;

    domSanitizer = TestBed.inject(DomSanitizer);
    matIconRegistry = TestBed.inject(MatIconRegistry);
    setMatIcons(matIconRegistry, domSanitizer);

    fixture.detectChanges();
  });

  it('should create', () => expect(component).toBeTruthy());
});

import { MenuItem } from './menu.interface';

export const MenuItems: MenuItem[] = [
  {
    label: 'MENU.COMPANY',
    route: '/app',
    icon: 'apartment'
  },
  {
    label: 'MENU.EMPLOYEES',
    route: '/app/employees',
    icon: 'groups'
  }
];
